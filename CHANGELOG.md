2.02
- opened the option for you to test a copy of your world in a Node.js server 
running FoundryVTT v11. Do not replace your v10 install with v11 yet and do 
not test it with the only copy of your world data. There is no going back 
from this migration of your data.
- I have done this and encountered no issues. I would like you to try and let 
me know if you have any.
