# Shadows of the Northern Woods

Shadows of the Northern Woods is the adventure included with the core rules for Against the Darkmaster by Open-Ended Games. This module has been converted to work with Against the Darkmaster on Foundry VTT.

Powered by Open00 is a Open Ended Games, Inc copyright.

***

## Name
Shadows of the Northern Woods

## Description
Shadows of the Northern Woods is the adventure included with the core rules for Against the Darkmaster by Open-Ended Games. This module has been converted to work with Against the Darkmaster on Foundry VTT.

## Installation
Copy the following link to the Manifest file and install the module in Foundry manually.

https://gitlab.com/jbhuddleston/shadows-of-the-northern-woods/-/raw/main/module.json

## System Requirements
The following Foundry VTT Modules were used in the development of this Module.

### Required Modules

1. Stairways (Teleporter)

### Optional Modules

1. About Face
2. Compendium Scene Viewer
3. Dice So Nice!
4. Dice Tray
5. Perfect Vision
6. TorchLight
7. Michael Ghelfi Audio Pack

## Support
Help with game rules: Against the Darkmaster Discord
Help with Foundry VTT: Foundry VTT Discord
Help with Against the Darkmaster system: Against the Darkmaster Discord

## Contributing
Contributions are welcome for identification of errors or omissions or provision of copyright-free token art.

## Authors and acknowledgment

### Rule System and Adventure
[Open Ended Games](https://www.vsdarkmaster.com/) produced the adventure Shadows of the Northern Woods for their game Against the Darkmaster. Kergan converted the adventure for use in Foundry VTT.

### Virtual Table Top
[Foundry VTT](https://foundryvtt.com)

### Game System Implementation for Foundry VTT
James Huddleston: Against the Darkmaster system development & Packaging the Module

### Battlemaps
[Jon Hodgson](https://www.patreon.com/jonhodgsonmaps), [Forgotten Adventures](https://www.forgotten-adventures.net), [Tom Cartos](https://www.patreon.com/tomcartos): Assets used to create maps
[DungeonDraft](https://dungeondraft.net): Mapping software
[ZAT](https://ko-fi.com/zatnikotel): Wilderness Battlemaps
Kergan produced the battlemaps described within the adventure using DungeonDraft.

## License
Shadows of the Northern Woods Conversion for Foundry VTT is an independent production by Kergan and is not affiliated with Open Ended Games, Inc. It is published under the Powered by Open00 Third Party License.

## Project status
Draft

